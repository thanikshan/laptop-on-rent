/*
    Author: Sreyas Naaraayanan Ramanathan
*/

let jwt = require('jsonwebtoken');
let config = require('../../config/database.config.js');
var CryptoJS = require("crypto-js");


var Auth = require('../models/auth.model')
var user = require('../models/getProfileDetails.model')
exports.login = (req, res) => {
  // For the given username fetch user from DB
  let username = req.body.username;
  let password = req.body.password;

  // let mockedUsername = 'admin';
  // let mockedPassword = 'password';
 
  //User Login -Sreyas ,Admin Login -Hari
  Auth.getUserbyEmail(username, function (err, task) {
    if (err) {
      res.send(err);
    }
    if (task) {
      //res.json(task);
      if (task.length > 0) {

        user.fetchProfileDetails(task[0].uId,function (err, lock) {
          if (err) {
            res.send(err);
          } else {
            if (lock[0].isLocked == 0) {
              userPassword = task[0].pswd


              var bytes = CryptoJS.AES.decrypt(userPassword, 'cipher1234');
              var originalPassword = bytes.toString(CryptoJS.enc.Utf8);
              userPassword = task[0].pswd
              let uId = task[0].uId
              
              if (originalPassword) {
                if (password === originalPassword) {
                  let token = jwt.sign({ username: username },
                    config.secret,
                    {
                      expiresIn: '720h' // expires in 24 hours
                    }
                  );
                  // return the JWT token for the future API calls
                  res.json({
                    success: true,
                    message: 'Authentication successful!',
                    token: token,
                    uId: uId,
                    username: username
                  });
                } else {
                  res.json({
                    success: false,
                    message: 'Incorrect Email ID or Password'
                  });
                }
              }
            } else {
              res.json({
                success: false,
                message: 'The account is locked'
              });
            }
          }
        })

      } else {
        res.json({
          success: false,
          message: 'Email does not exist'
        });
      }
    }
    else {
      res.send(400).json({
        success: false,
        message: 'Authentication failed! Please check the request'
      });
    }

  });
}

exports.index = (req, res) => {
  res.json({
    success: true,
    message: 'Index page'
  });
}
// Verify JWT token Hari
exports.checkToken = (req, res, next) => {
  let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase

  if (token) {
    if (token.startsWith('Bearer ')) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }

    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.json({
      success: false,
      message: 'Auth token is not supplied'
    });
  }
};
