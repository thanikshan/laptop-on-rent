
/*
    Author: Sreyas Naaraayanan Ramanathan
*/

var Email = require('../models/email.model')
exports.sendEmail = (req, res) => {
  let username = req.body.username;
  var otp = (Math.floor(Math.random() * 10000) + 10000).toString().substring(1);

  // Send OTP to email for password reset
  Email.sendEmail(username, otp, function (err, task) {
    if (err) {
      res.send(err);
    }
    if (task) {
      res.json({
        success: true,
        message: 'Email Sent'
      });
    } else {
      res.json({
        success: false,
        message: 'Sorry Error in sending OTP'
      });
    }


  });

}