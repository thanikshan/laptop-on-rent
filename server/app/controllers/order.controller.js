/*
    Author: Srikrishna Sasidharan
*/

let jwt = require('jsonwebtoken');
let config = require('../../config/database.config.js');


var OrderModel = require('../models/order.model')
let CartModel = require('../models/cart.model');

// place order for user from cart items
exports.placeOrder = (req,res)=>{


    class Order{
        
        constructor(){
            this.uid =  req.body.uid;
            this.orderid =  req.body.orderid
            this.orderstate =  req.body.orderstate
            this.totalAmount =  req.body.totalAmount
            this.paymentid =  req.body.paymentid
            this.paymentStatus =  req.body.paymentStatus
            this.orderdate =  req.body.orderdate
            this.addressid =  req.body.addressid
            this.orderline=  req.body.orderline 
            this.inventoryId =  req.body.inventoryId
            this.quantity=  req.body.quantity
            this.amount  =  req.body.amount
            this.fromdate =  req.body.fromdate
            this.todate =  req.body.todate

        }
    }
    order = new Order();
    var cartItems=[]
    CartModel.getCartDetailsForUser(order , function(errror, rresponse){
                if(rresponse){
                
                    for (var i = 0; i < rresponse.length; i++){
                        cartItems[i] = rresponse[i]
                    }

                OrderModel.placeOrder(order, function(err, result){
                if(result){
                    var statusCount = 0;
                    order.orderid = result
                    if(cartItems != undefined){
                        cartItems.forEach(element => {
                        
                            OrderModel.placeOrderLine(element,result, function(error,res){
                                if(res){
                                    statusCount +=1
                                }
                                
                            });
                            
                            
                        });
                    }
                    res.json({
                        status: true,
                        message:"Order placed successfully",
                        orderid : result
                    })
                    
                }else{
                    res.json({
                        status: false,
                        message:"error while placing order"
                    })
                }
                    })

                 
                }
        });

    
        

        

    


}