/*
    Author: Sreyas Naaraayanan Ramanathan,Hari Arundchalam
*/

let config = require('../../config/database.config.js');

var Detail = require('../models/getProfileDetails.model');
exports.fetchProfile = (req, res) => {

    let uid = req.body.uid;
    //Fetch Profile details from db
    Detail.fetchProfileDetails(uid, function (err, task) {

       
        if (err) {

            res.send(err);
        }

        if (task) {

            res.json({
                success: true,
                message: 'Details fetched successfully',
                data:task

            });
        }
        else {
            res.json({
                success: false,
                message: 'Error occured while fetching details'
            });
        }


    });


}

exports.adminProfile = (req, res) => {

    //let uid = req.body.uid;

    //fetch profile details -admin

    Detail.fetchProfiles( function (err, task) {

       
        if (err) {

            res.send(err);
        }

        if (task) {

            res.json({
                success: true,
                message: 'Details fetched successfully',
                data:task

            });
        }
        else {
            res.json({
                success: false,
                message: 'Error occured while fetching details'
            });
        }


    });


}