const Check = require('../models/check.model.js');

// Create and Save a new check
exports.create = (req, res) => {

};

// Retrieve and return all checks from the database.
exports.findAll = (req, res) => {
 Check.find({})
    .then(checks => {
        res.send(checks);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

// Find a single check with a checkId
exports.findOne = (req, res) => {

};

// Update a check identified by the checkId in the request
exports.update = (req, res) => {

};

// Delete a check with the specified checkId in the request
exports.delete = (req, res) => {

};