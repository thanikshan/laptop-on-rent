/*
    Author: Thanigaiselvan Sentil Shanmugam
*/

let config = require('../../config/database.config.js');
var landingProducts = require('../models/landing.model')
let gamingProducts;
let studentProducts;


exports.landing = (req, res) => {
    //Retrieve gaming products from DB
    landingProducts.getGamingProducts(function (err, result){
        if(err == null)
        {
            gamingProducts=result;  
        }
        else
        {
            console.log(err);
        }
        
    });
    //Retrieve student products from DB
    landingProducts.getStudentProducts(function (err, result){
        if(err == null)
        {
            studentProducts=result;
             
        }
        else
        {
            console.log(err);
        }
        
    });
    //Retrieve business products from DB
    landingProducts.getBusinessProducts(function (err, result){
        if(err == null)
        {
            res.json({
                success: true,
                message: 'Authentication successful!',
                gamingProducts: gamingProducts,
                studentProducts: studentProducts,
                businessProducts: result

              }); 
        }
        else
        {
            console.log(err);
        }
        
    });

}   
