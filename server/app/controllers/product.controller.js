/*
    Author: Thanigaiselvan senthil shanmugam,Hari Arunachalam
*/

let config = require('../../config/database.config.js');
let productResult;
var productDetails = require('../models/product.model')
var quantityDetails= require('../models/product.model')
exports.product = (req, res) => {
    let productId = req.params.id;

    productDetails.getProductDetails(productId, function (err, result){
        
        if(err ==null)
        {
            productResult=result;
        }
        else
        {
            console.log(err);
        }
        
    });

    productDetails.getFromDate(productId, function (err, result){
        
        if(err ==null)
        {
            res.json({
                success: true,
                message: 'Authentication successful!',
                fromDate: result,
                productDetails: productResult
              });
        }
        else
        {
            console.log(err);
        }
        
    });

    


};
exports.getProductID=(req,res)=>{
    let productId = req.body.id;

    productDetails.getProductID(productId, function (err, result){
        
        if(err ==null)
        {
            res.json({
                success: true,
                message: 'lol',
                productID: result,
              });
        }
        else
        {
            console.log(err);
        }
        
    });


}

exports.quantity=(req,res)=>{
    let productId = req.params.id;

    quantityDetails.getQuantity(productId, function (err, result){
        
        if(err ==null)
        {
            res.json({
                success: true,
                message: 'Authentication successful!',
                quantity: result,
              });
        }
        else
        {
            console.log(err);
        }
        
    });


}
//Autosuggestion
exports.search=(req,res)=>{
    let text = req.body.text;
    let start = req.body.start;
    let end = req.body.end;
    productDetails.search(text,start,end, function (err, result){
        
        if(err ==null)
        {
            //result = result[1]
            productResult=result;
            return res.json({
                result
            })
        }
        else
        {
            console.log(err);
        }
        
    });
}
//search with filter
    exports.filter=(req,res)=>{
        let text = req.body.searchText;
        let processors = req.body.processors;
        let ram = req.body.ram;
        let memory = req.body.memory;
        let resolution = req.body.resolution;
        let Company = req.body.Company;
        let start = req.body.start;
        let end = req.body.end;
       
        productDetails.filter(text,processors,ram,memory,resolution,Company,start,end, function (err, result){
             
             if(err ==null)
             {  
                count = result[0]
                result = result[1]
                 productResult=result;
                 return res.json({
                     result,
                     count
                 })
             }
             else
             {
                 console.log(err);
             }
             
         });
           
}
//add laptop
exports.addProduct=(req,res)=>{

    productDetails.addProduct(req, function (err, result){
        
        if(err ==null)
        {
            //result = result[1]
            productResult=result;
            return res.json({
                result
            })
        }
        else
        {
            console.log(err);
        }
        
    });
}