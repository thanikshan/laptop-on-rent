/*
    Authors: Srikrishna Sasidharan
*/
var sql = require('../../db.js');

var Order = function(order){
    this.uid = order.uid
    this.orderid = order.orderid
    this.orderstate = order.orderstate
    this.totalAmount = order.totalAmount
    this.paymentid = order.paymentid
    this.paymentStatus = order.paymentStatus
    this.orderdate = order.orderdate
    this.addressid = order.addressid
    this.orderline= order.orderline 
    this.inventoryId = order.inventoryId
    this.quantity= order.quantity
    this.amount  = order.amount
    this.fromdate = order.fromdate
    this.todate = order.todate
}


Order.placeOrder = function(order,result){

    sql.query("select max(orderid) as m from orders", function (err, res) {             
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            var orderID ;
            if(res[0].m == null || res[0].m == undefined){
                orderID = 1;
            }else{
             orderID = res[0].m + 1
            }
            que = "insert into orders(orderid,orderstate,totalAmount,uid,paymentid,paymentStatus,orderdate,addressid)"
            + "values("+orderID+",'Placed',"+order.totalAmount+","+order.uid+","+order.paymentid+",'"
            +order.paymentStatus+"',CURRENT_TIMESTAMP,"+order.addressid+")";
            sql.query(que, function (error, response) {
                if(error){
                    console.log(error)
                    result(error,null)
                }else{
                    result(null,orderID);
                }
            })
        }
    }
    )
}

//to be looped for each cart product
Order.placeOrderLine = function (order ,orderid, result) {
    que = ""
    sql.query("select inventoryID from inventory where productID = "+order.productID
    +" and toDate is null limit "+ order['quantity'], function(err,res){
        if(err){
            console.log(err)
            result(err,null)
        }else{
            var inventoryIds = []
            for (var i=0;i<res.length;i++){
                 inventoryIds[i] = res[i].inventoryID
             }
            // inventoryIds = res
            que = ""
            inventoryIds.forEach(id =>{
                const isoDateString = order['fromDate'].toISOString();
                const isoDate = new Date(isoDateString);
                const mySQLFromDateString = isoDate.toJSON().slice(0, 19).replace('T', ' ');
                const isoToDateString = order['toDate'].toISOString();
                const isoToDate = new Date(isoDateString);
                const mySQLToDateString = isoDate.toJSON().slice(0, 19).replace('T', ' '); 
                que += "insert into orderdetails(orderline,orderId,inventoryId ,quantity , amount , fromdate , todate ) "+
                "values ("+order['cartid']+","+orderid+","+id+","+order['quantity']+","+order['perDay']+", '"
                +mySQLFromDateString+"', '"+mySQLToDateString+"');"
                que += "update inventory set toDate= '"+mySQLToDateString+"' where inventoryId = "+id+";"
            })
            if(que!=""){
                sql.query(que, function(error, response){
                    if(error){
                        console.log(error)
                        result(error,null)
                    }else{
                        querr = ""
                        querr = "select softwareid from software where softwaredesc in ('"
                        var softs = order['softwares'].split(",")
                        for (var i = 0; i< softs.length; i++){
                            if(i>0){
                                querr += ",'"
                            }
                            querr +=  softs[i]+"'"
                        }
                        querr += ")"
                    sql.query(querr, function(error, response){
                    if(error){
                        console.log("error :",error);
                        result(error,null);
                    }else{
                        //result(null,response);
                        swId = response[0].softwareid;
                        arraySoftwares = response;
                    
                        
                        if (arraySoftwares!= undefined){
                           
                      
                        let q =""
                        for(var j=0;j<arraySoftwares.length;j++){
                            q += "insert into ordersoftware(orderline ,softwareid) values('"+  order['cartid']+"','"+arraySoftwares[j].softwareid+"'); "; 
                        }
                        sql.query(q, function(err, res){
                        if(err){
                            console.log("error :",err);
                            result(err,null);
                        }else{

                            result(null,res);
                        }
                        });
                    }
                    
                    }
                });


                    }
                });
            }

            //result(null,res);
        }
    });

}

module.exports= Order;
