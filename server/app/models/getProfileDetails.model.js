/*
Authors : Sreyas Naaraayanan,Hari Arunachalam 
*/
var sql = require('../../db.js');
let config = require('../../config/database.config.js');



var Details = function(details){
    this.uid = details.uid;
}

Details.fetchProfileDetails =function(uid,result){

    sql.query("Select firstName,lastName,email,addressLine1,addressLine2,city,state,postcode,phoneNumber,uId,isAdmin,isLocked from user where uId = ? ", uid, function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else {
            
            result(null, res);

        }
    });
}

Details.fetchProfiles =function(result){

    sql.query("Select * from user", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else {
            
            result(null, res);

        }
    });
}
module.exports = Details;