/*
Authors : Sreyas Naaraayanan,Hari Arunachalam 
*/
var sql = require('../../db.js');
let config = require('../../config/database.config.js');

var EditDetails = function (details) {

    this.uId = details.uId;
    this.firstName = details.firstName;
    this.lastName = details.lastName;
    this.addressLine1 = details.addressLine1;
    this.addressLine2 = details.addressLine2;
    this.phoneNumber = details.phoneNumber;
    this.city = details.city;
    this.state = details.state;
    this.postcode = details.postcode;
}

EditDetails.editProfileDb = function (details, result) {

    
    

    var q1 = "UPDATE user set firstName = '" + details.firstName + "', lastName ='" + details.lastName 
    + "',addressLine1 ='" + details.addressLine1 + "',addressLine2 ='" + details.addressLine2 + "',city ='"
     + details.city + "', state ='" + details.state + "',postcode='" + details.postcode + "', phoneNumber ='" 
     + details.phoneNumber + "' where uId = "+details.uId+ "";

    sql.query(q1,function (err, res) {

        if (err) {
            console.log("error: ", err);
            result(err, null);
        }
        else {
            console.log("User details update successfully");
            result(null, details);

        }
    });


}

//Admin edit user
EditDetails.lockUser = function (uId,isLocked, result) {

    
    if(isLocked == true){
        isLocked = 0
    }else{
        isLocked = 1
    }
    var q1 = "UPDATE user set isLocked = " + isLocked +" where uId = "+uId+ ";";

    sql.query(q1,function (err, res) {

        if (err) {
            console.log("error: ", err);
            result(err, null);
        }
        else {
            console.log("User details update successfully");
            result(null, res);

        }
    });


}
module.exports = EditDetails;