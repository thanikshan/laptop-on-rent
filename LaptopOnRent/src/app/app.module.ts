import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserNavigationBarComponent } from './user-navigation-bar/user-navigation-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ProductPageComponent } from './product-page/product-page.component';
import { StarRatingComponent } from './star-rating/star-rating.component';
import { OrdersComponent, DialogOverviewExampleDialog } from './orders/orders.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CartComponent, AddToCartDialog } from './cart/cart.component';
import { CartStepperComponent } from './cart-stepper/cart-stepper.component';
import { ChoosePaymentComponent } from './choose-payment/choose-payment.component';
import { ChooseAddressComponent, AddAddressDialog } from './choose-address/choose-address.component';
import { OrderConfirmationComponent } from './order-confirmation/order-confirmation.component';
import { AddLaptopComponent, DialogOverviewExample } from './add-laptop/add-laptop.component';
import {ReviewDialog} from './product-page/review/review.dialog'

//services
import {AuthService} from './services/authentication-service';


import {
  MatButtonModule,
  MatIconModule,
  MatListModule,
  MatSidenavModule,
  MatToolbarModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatOptionModule,
  MatSelectModule,
  MatTooltipModule,
  MatSnackBarModule,
  MatPaginatorModule,
  MatTableModule,
  MatSlideToggleModule,
  MatStepperModule,
  MatRadioModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatCardModule,
  MatBadgeModule,
  MatAutocompleteModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  

  
} from '@angular/material';

import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AdminUserDetailsComponent } from './admin-user-details/admin-user-details.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CustomHttpInterceptor } from './services/http-interceptor';
// import { SpinnerService } from './services/SpinnerService';
import { LandingService } from './services/landingService';
import { SearchService } from './services/search-services';
import { ProductService } from './services/productService';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UpdatePasswordDbComponent } from './update-password-db/update-password-db.component';
import { PaymentService } from './services/payment-service';
import { AdminService } from './services/admin.services';

@NgModule({
  declarations: [
    AppComponent,
    UserNavigationBarComponent,
    LandingPageComponent,
    ProductPageComponent,
    StarRatingComponent,
    AdminUserDetailsComponent,
    LoginComponent,
    RegisterComponent,
    OrdersComponent, 
    DialogOverviewExampleDialog,
    UserProfileComponent,
    CartComponent,
    CartStepperComponent,
    ChoosePaymentComponent,
    ChooseAddressComponent,
    OrderConfirmationComponent,
    SearchResultComponent,
    AdminHomeComponent,
    AddLaptopComponent,
    ReviewDialog,
    AddToCartDialog,
    AddAddressDialog,
    ResetPasswordComponent,
    AddToCartDialog,
    UpdatePasswordDbComponent,
    DialogOverviewExample
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    MatToolbarModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatTableModule,
    MatSlideToggleModule,
    FormsModule,
    //ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    ReactiveFormsModule,
    MatStepperModule,
    MatRadioModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatBadgeModule,
    HttpClientModule,
    MatAutocompleteModule,
    

  ],
  entryComponents: [DialogOverviewExampleDialog,ReviewDialog,AddToCartDialog,AddAddressDialog,DialogOverviewExample],
  providers: [
    
    {provide: HTTP_INTERCEPTORS,
      useClass: CustomHttpInterceptor,
      multi: true},AuthService,
      LandingService,ProductService,PaymentService,
	  SearchService,AdminService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
