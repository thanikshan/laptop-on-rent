/*
Authors : Srikrishna Sasidharan 
*/

import { HttpHeaders, HttpClient ,HttpErrorResponse} from '@angular/common/http';
import { Observable,throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
  
  export class CartService{
    apiUrl: string = 'http://54.162.42.117:3000';
    headers = new HttpHeaders().set('Content-Type', 'application/json');
  
    constructor(private http: HttpClient) { }

    //addtocart
    addToCart(data) : Observable<any>{
        let API_URL = this.apiUrl+'/addToUserCart'
        return this.http.post(API_URL, data)
        .pipe(
          catchError(this.error)
        )
    }


    //fetchCartItemsForUser
    fetchCartItemsForUser(data): Observable<any>{
      let API_URL = this.apiUrl+'/getCartDetailsForUser'
      return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
    }

    //removeCartItem
    removeCartItem(data): Observable<any>{
      let API_URL = this.apiUrl+'/removeCartItem'
      return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
    }

    //updateCart
    updateCart(data) : Observable<any>{
      let API_URL = this.apiUrl+'/updateCartItem'
      return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
    }
    //updateSoftware
    updateSoftware(data) : Observable<any>{
      let API_URL = this.apiUrl+'/updateSoftware'
      return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
    }


    getCartCount(data): Observable<any>{
      let API_URL = this.apiUrl+'/getCartCount'
      return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
    }

    getPromotions(): Observable<any>{
      let API_URL = this.apiUrl+'/getPromotions'
      return this.http.get(API_URL)
      .pipe(
        catchError(this.error)
      )
    }
    // Handle Errors 
  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

  
  }