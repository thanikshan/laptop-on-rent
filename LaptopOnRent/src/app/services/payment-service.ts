/*
Authors : Srikrishna Sasisdharan, Sreyas Naaraayanan Ramanathan 
*/
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class PaymentService {

  apiUrl: string = 'http://54.162.42.117:3000';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }
  //addPayment
  addPayment(data): Observable<any> {
    let API_URL = this.apiUrl + '/addNewPayment'
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }



  getAllPayments(data): Observable<any> {
    let API_URL = this.apiUrl + '/getUserPaymentMethods'
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }


  placeOrder(data): Observable<any> {
    let API_URL = this.apiUrl + '/placeOrder'
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  getPastOrders(data): Observable<any> {
    let API_URL = this.apiUrl + '/allOrders'
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }



  // Handle Errors 
  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(errorMessage);
  }


}