/*
Authors : Sreyas Naaraayanan,Hari Arunachalam 
*/
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  apiUrl: string = 'http://54.162.42.117:3000';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  // Create
  login(data): Observable<any> {
    let API_URL = this.apiUrl + '/login'
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  registration(data): Observable<any> {
    let API_URL = this.apiUrl + '/registerUser'
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }
  sendEmail(data): Observable<any> {
    let API_URL = this.apiUrl + '/send-email'
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  updatePassword(data): Observable<any> {
    let API_URL = this.apiUrl + '/updatePwd'
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  fetchProfileDetails(data): Observable<any> {
    let API_URL = this.apiUrl + '/fetchProfile'
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  editProfileDb(data): Observable<any> {
    let API_URL = this.apiUrl + '/editProfileDb'
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  // Handle Errors 
  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(errorMessage);
  }

}