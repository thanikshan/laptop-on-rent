/*
Authors : Thanigaiselvan Senthil Shanmugham
*/
import { HttpHeaders, HttpClient ,HttpErrorResponse} from '@angular/common/http';
import { Observable,throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
  
  export class LandingService{
    apiUrl: string = 'http://54.162.42.117:3000';
    headers = new HttpHeaders().set('Content-Type', 'application/json');
  
    constructor(private http: HttpClient) { }

    landing() : Observable<any>{
        let API_URL = this.apiUrl+'/landing'
        return this.http.get(API_URL)
        .pipe(
          catchError(this.error)
        )
    }



    // Handle Errors 
  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    
    return throwError(errorMessage);
  }

  
  }