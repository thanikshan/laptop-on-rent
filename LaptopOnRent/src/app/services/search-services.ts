/*
Authors : Hari Arunachalam 
*/
import { HttpHeaders, HttpClient ,HttpErrorResponse} from '@angular/common/http';
import { Observable,throwError } from 'rxjs';
import { catchError, map,switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
  
  export class SearchService{
    apiUrl: string = 'http://54.162.42.117:3000';
    headers = new HttpHeaders().set('Content-Type', 'application/json');
  
    constructor(private http: HttpClient) { }

    //addtocart
    search(body) : Observable<any>{
        let API_URL = this.apiUrl+'/productSearch';
        return this.http.post(API_URL,body)
        .pipe(
          
          
        )
    }

    getProductID(body) : Observable<any>{
      let API_URL = this.apiUrl+'/productID';
      return this.http.post(API_URL,body)
      .pipe(
        
        
      )
  }

    filter(body) : Observable<any>{
      let API_URL = this.apiUrl+'/filter';
      return this.http.post(API_URL,body)
      .pipe(
        
        
      )
  }



    // Handle Errors 
  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }    
    return throwError(errorMessage);
  }

  
  }