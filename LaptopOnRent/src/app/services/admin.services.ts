/*
Authors : Hari Arunachalam 

*/
import { HttpHeaders, HttpClient ,HttpErrorResponse} from '@angular/common/http';
import { Observable,throwError } from 'rxjs';
import { catchError, map,switchMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
  
  export class AdminService{
    apiUrl: string = 'http://54.162.42.117:3000';
    headers = new HttpHeaders().set('Content-Type', 'application/json');
  
    constructor(private http: HttpClient) { }

    //add a laptop to the database
    addProduct(body) : Observable<any>{
        let API_URL = this.apiUrl+'/addProduct';
        return this.http.post(API_URL,body)
        .pipe(
          
          
        )
    }
    //get users from database
    getUsers() : Observable<any>{
      let API_URL = this.apiUrl+'/getUsers';
      return this.http.get(API_URL)
      .pipe(
        
        
      )
  }
  //Lock and unlock users
  lockUser(body) : Observable<any>{
    let API_URL = this.apiUrl+'/lockUser';
    return this.http.post(API_URL,body)
    .pipe(
      
      
    )
}

       // Handle Errors 
    error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

  
  }