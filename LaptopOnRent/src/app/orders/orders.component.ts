/*
  Authors: Sreyas Naaraayanan Ramanathan
*/
import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ReviewDialog } from '../product-page/review/review.dialog';
import { PaymentService } from '../services/payment-service';
import { MatSnackBar } from '@angular/material';
import { MatFormFieldModule, MatNativeDateModule, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  pastOrders = [];
  currentOrders = [];
  reviewForms: FormGroup;
  onInit = false;
  startDate = new Date(1990, 0, 1);
  starCount: number = 5;
  constructor(public dialog: MatDialog, private route: Router, private allOrders: PaymentService, private activateRoute: ActivatedRoute, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.onInit = false;
    var uid = sessionStorage.getItem('uid');

    this.allOrders.getPastOrders({ 'uid': uid }).subscribe(data => {

      if (data.success) {
        data.data.forEach(element => {
          if (element.orderstate == 'Returned') {
            this.pastOrders.push(element);
          }

          else if (element.orderstate == 'Placed') {
            this.currentOrders.push(element);
          }
        });

      }
      else {

        this._snackBar.open(data.message, "No Order History ", {
          duration: 1500,
        });
      }

    })

    this.reviewForms = new FormGroup({
      'review': new FormControl('', { validators: [Validators.required] })
    }
    )

  }
  onSave() {
    if (this.reviewForms.invalid) {
      this.onInit = true;

    }
    else {
      this.route.navigate(["", "order"]);
    }

  }

  openDialogs(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: 'auto',
      height: 'auto',

    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }
  navProduct(prodID) {

    this.route.navigate(["","product",prodID])

    //this.route.navigate(["", "product/:id"]);
  }

  openDialog(prodID): void {

    if (sessionStorage.getItem('uid') == null)
    {
      this.route.navigate(["","login"])
    }
  else {
  const dialogRef = this.dialog.open(ReviewDialog, {
   
    data: {starCounting: this.starCount,'ProductCount':prodID}
  });

  dialogRef.afterClosed().subscribe(result => {
    //this.animal = result;
  });
}
}

}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {
  savebtn;
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: OrdersComponent) { }


  saveData() {
    this.savebtn = document.getElementById("date");
    this.dialogRef.close();

  }
  closeModel() {
    this.dialogRef.close();
  }


 

}