/*
Authors : Hari Arunachalam 
Method names are descriptive
*/
import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {SearchService} from '../services/search-services';
import { FormGroup, FormControl, FormArray } from "@angular/forms";
import * as _ from "lodash";
import {PageEvent} from '@angular/material/paginator';
import {  MatDialog } from '@angular/material';
import  {AddToCartDialog} from '../cart/cart.component';
@Component({
  selector: 'app-search-result', 
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

  selected ="1";
  expand= false;
  products;
  searchText;
  
  pageEvent:PageEvent
  count = 15
    constructor(public dialog: MatDialog,private route:Router,private searchService:SearchService,private activateRoute:ActivatedRoute) {
      this.searchText = this.activateRoute.snapshot.paramMap.get("id");
     }
    step = 0;
  
    setStep(index: number) {
      this.step = index;
      this.expand = true;
    }
  
    nextStep() {
      this.step++;
    }
  
    search(pg){
      this.expand = false;
      this.step = 0
      for(let i=0;i<4;i++){
       // if(this.p[i].isChecked())
      }
      this.getSelectedFilters()
      console.log(this.selectedProcessorName)
      console.log(this.selectedRamName)
      console.log(this.selectedMemoryName)
      console.log(this.selectedResolutionName)
      console.log(this.selectedCompanyName)

      let body ={
        'searchText':this.searchText,
        'processors':this.selectedProcessorName,
        'ram':this.selectedRamName,
        'memory':this.selectedMemoryName,
        'resolution':this.selectedResolutionName,
        'Company':this.selectedCompanyName,
        'start':10,
        'end':pg
      }

      console.log(body)
      this.searchService.filter(body).subscribe(data=>{
        console.log(data)
        this.products = data.result;
        let c = data.count[0]
        c= c.c
        console.log(c)
        this.count = c;
      })
    }

    goProduct(prod){
      this.route.navigate(["/","product",prod])
    }
    ngOnInit() {
      // this.searchService.search(this.searchText,"0","10").subscribe(data=>{
      //   console.log(data)
      //   this.products = data.result;
      // })
      this.createFormInputs();
      this.search(0)
      
    }

    refresh(){
      this.getSelectedFilters()
      console.log(this.selectedProcessorName)
    }
  processorForm: FormGroup;
  processors =[
    {name: "i7",
    value: "i7",
    },
    {name: "i5",
    value: "i5",
    },
    {name: "i3",
    value: "i3",
    },
    {name: "dual",
    value: "dual",
    },]

  ramForm: FormGroup;
  rams =[
    {name: "4gb",
    value: "4GB",
    },
    {name: "8gb",
    value: "8GB",
    },
    {name: "16gb",
    value: "16GB",
    },
   ]

  memoryForm: FormGroup;
  memory =[
    {name: "256gb",
    value: "256GB",
    },
    {name: "512gb",
    value: "512GB",
    },
    {name: "1tb",
    value: "1TB",
    },
   ]

  resolutionForm: FormGroup;
  resolutions =[
    {name: "720p",
    value: "720",
    },
    {name: "1080p",
    value: "1080",
    },
    {name: "4k",
    value: "4K",
    },
   ]

  companyForm: FormGroup;
  company =[
    { name: "apple",
      value: "APPLE",
    },
    {name: "dell",
    value: "DELL",
    },
    {name: "lenovo",
    value: "LENOVO",
    },
   ]
   
    createFormInputs() {
      this.processorForm = new FormGroup({
        processors: this.createControls(this.processors)
      });

      this.ramForm = new FormGroup({
        rams: this.createControls(this.rams)
      });

      this.memoryForm = new FormGroup({
        memory: this.createControls(this.memory)
      });

      this.resolutionForm = new FormGroup({
        resolutions: this.createControls(this.resolutions)
      });
      this.companyForm = new FormGroup({
        company: this.createControls(this.company)
      });
    }
    createControls(inp) {
      const arr = inp.map(hobby => {
        return new FormControl( false);
      });
      return new FormArray(arr);
    }


    selectedProcessors
    selectedRams
    selectedMemory
    selectedResolutions
    selectedCompany
    getSelectedFilters() {
      this.selectedProcessors = _.map(this.processorForm.controls.processors['controls'], (box, i) => {
        return box.value && this.processors[i].value;
      });

      this.selectedRams = _.map(this.ramForm.controls.rams['controls'], (box, i) => {
        return box.value && this.rams[i].value;
      });
      this.selectedMemory = _.map(this.memoryForm.controls.memory['controls'], (box, i) => {
        return box.value && this.memory[i].value;
      });

      this.selectedResolutions = _.map(this.resolutionForm.controls.resolutions['controls'], (box, i) => {
        return box.value && this.resolutions[i].value;
      });

      this.selectedCompany = _.map(this.companyForm.controls.company['controls'], (box, i) => {
        return box.value && this.company[i].value;
      });

      this.getSelectedFilterNames();
    }
    selectedProcessorName
    selectedRamName
    selectedMemoryName
    selectedResolutionName
    selectedCompanyName
    getSelectedFilterNames() {
      this.selectedProcessorName = _.filter(this.selectedProcessors, function (item) {
        if (item !== false) {
          return item;
        }
      });

      this.selectedRamName = _.filter(this.selectedRams, function (item) {
        if (item !== false) {
          return item;
        }
      });

      this.selectedMemoryName = _.filter(this.selectedMemory, function (item) {
        if (item !== false) {
          return item;
        }
      });

      this.selectedResolutionName = _.filter(this.selectedResolutions, function (item) {
        if (item !== false) {
          return item;
        }
      });

      this.selectedCompanyName = _.filter(this.selectedCompany, function (item) {
        if (item !== false) {
          return item;
        }
      });
    }

   
    getPaginatorData(event){
    console.log(event)
      this.search(event.pageIndex * 10)
   }


   addToCartFromExplore(prod: any){

    // var cartCount =+localStorage.getItem('cartCount');
    // cartCount = cartCount + 1;
    // localStorage.setItem('cartCount',''+cartCount);
    // this.cartItem.emit(cartCount);
  
   
      const dialogRef = this.dialog.open(AddToCartDialog, {
        width: 'auto',
        height: 'auto',
     
      data: { prodID: prod.productID, Product: prod.Product,
        Company:prod.Company,  perDay: prod.perDay, productImg: prod.productImg,
        maxQuantity: "1"
      }
      });
  
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        this.route.navigate(["/","cart"])
        //this.ngOnInit();
       // this.animal = result;
      });
    

  }
    
    
}
