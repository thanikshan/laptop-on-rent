/*
  Authors: Srikrishna Sasidharan
*/
import { Component, OnInit, Inject, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { DialogData } from '../product-page/review/review.dialog';
import { FormControl } from '@angular/forms';
import { AddressService } from '../services/address-service';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-choose-address',
  templateUrl: './choose-address.component.html',
  styleUrls: ['./choose-address.component.css']
})
export class ChooseAddressComponent implements OnInit {
  firstName: String;
  lastName: String;
  contact: Number;
  address: String;
  address2: String;
  city: String;
  state: String;
  pcode: String;
  isAddress1Removed:boolean=true;
  isAddress2Removed:boolean=true;
  constructor(public dialog: MatDialog,private _route:Router, private addressService: AddressService, public activatedRoute: ActivatedRoute) { }
  emailTest:boolean;
  addresses: [];
  checkAddressVal= false;
  addressSelected;
  selectedAddress;
  validateProceed =false;
  state$: any;
  data:any;
  delivery;
  
  del;
  ngOnInit() {
    this.delivery =sessionStorage.getItem('scheduledDelivery').toString().trimRight()
     this.del = this.delivery.toString().split(" ")
   
    this.data =  sessionStorage.getItem('price')
    this.firstName = "";
    this.lastName = "";
    this.contact = +1;
    this.address = "";
    this.city = "";
    this.state = "";
    this.pcode = "";
    this.emailTest= false;
    this.validateProceed = false;
    var uid = sessionStorage.getItem('uid')
    this.addressService.getAddress({"uid":uid}).subscribe(data=>{
      this.addresses = data.address;

      if(this.addresses.length>0){
        this.checkAddressVal = false;
      }else{
        this.checkAddressVal = true;
      }
    })
  }

  //opens add new address dialog
  openDialog(addr:any): void {
    const dialogRef = this.dialog.open(AddAddressDialog, {
      width: 'auto',
      height: 'auto',
      data: {addr:addr}
      //data: {name: this.name, animal: this.animal}
    });
    
    dialogRef.afterClosed().subscribe(result => {
      //this.animal = result;
      this.ngOnInit();
    });
  }

 



  // adds new address 
  addNewAddressConfirm(){

    if(  this.firstName == undefined || this.firstName == "" &&  this.lastName == undefined || this.lastName == "" &&  this.contact == undefined || this.contact.toString() == "" 
    &&  this.address == undefined || this.address == "" &&  this.city == undefined || this.city == "" &&  this.state == undefined || this.state == "" 
    &&  this.pcode == undefined || this.pcode == ""
    ){
      this.emailTest= true;

    }else{
      this.firstName = "";
      this.lastName = "";
      this.contact = +1;
      this.address = "";
      this.city = "";
      this.state = "";
      this.pcode = "";
  
      this._route.navigate(['/choose-payment']);
    }
    
    
  }
  
  closeModal(){

    this.firstName = "";
    this.lastName = "";
    this.contact = +1;
    this.address = "";
    this.city = "";
    this.state = "";
    this.pcode = "";
    
  }
 
  //remove user address
  RemoveClicked(uid: any, addressId: any){
    this.addressService.removeAddress({"uid":uid,"addressId":addressId}).subscribe(data=>{
      if(data.success){
        this.ngOnInit();
      }
    })

  }

  onItemChange(value){
 }

 //proceed to payment
  proceedPayment(){
  sessionStorage.setItem('selectedAddress',this.selectedAddress)

  if(this.selectedAddress == undefined){
    this.validateProceed = true;
  }else{
    this.addresses.forEach(data=>{
      if(data['addressId'] == this.selectedAddress){
        sessionStorage.setItem('selectedAddressFull',JSON.stringify(data))
      }
    });
    
    this.validateProceed = false;
    this._route.navigate(["/choose-payment"]);
  }

  }
}


@Component({
  selector: 'AddNewAddressDialog',
  templateUrl: 'AddNewAddressDialog.html',
})
export class AddAddressDialog implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AddAddressDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private addressService: AddressService) {}

    fName = new FormControl();
    lName = new FormControl();
    phone = new FormControl();
    address1 = new FormControl();
    address2 = new FormControl();
    city = new FormControl();
    state = new FormControl();
    postcode = new FormControl();

 
    checkValues = false;

    ngOnInit(){

    }
  onNoClick(): void {
    this.dialogRef.close();
  }

  // adds new address to user record
  addNewAddress(): void{
    if((this.fName.value==null || this.fName.value == "") &&
     (this.lName.value==null || this.lName.value == "") &&
     (this.phone.value==null || this.phone.value == "") &&
    (this.address1.value==null || this.address1.value == "")  &&
    (this.address2.value==null || this.address2.value == "")  &&
    (this.city.value==null || this.city.value == "")  &&
    (this.state.value==null || this.state.value == "")  &&
    (this.postcode.value==null || this.postcode.value == "")
      ){
        this.checkValues= true;
    }else{
      if(this.data['addr'] == 0){
        var uid = sessionStorage.getItem('uid')
      this.addressService.addAddress({"uid": uid,"firstname":this.fName.value,"lastname":this.lName.value,
      "addressline1": this.address1.value, "phone": this.phone.value,
      "addressline2":this.address2.value,"city":this.city.value,
      "state":this.state.value,"postcode":this.postcode.value }).subscribe(data=>{

        this.dialogRef.close();
      })
       }else{
      var userid = sessionStorage.getItem('uid')
      this.addressService.editAddress({"uid":userid,"addressId":this.data['addr'],"firstname":this.fName.value,"lastname":this.lName.value,
      "addressline1": this.address1.value, "phone": this.phone.value,
      "addressline2":this.address2.value,"city":this.city.value,
      "state":this.state.value,"postcode":this.postcode.value}).subscribe(data=>{
        this.dialogRef.close();

      })
    }
    }
  }

}

