import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePasswordDbComponent } from './update-password-db.component';

describe('UpdatePasswordDbComponent', () => {
  let component: UpdatePasswordDbComponent;
  let fixture: ComponentFixture<UpdatePasswordDbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePasswordDbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePasswordDbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
