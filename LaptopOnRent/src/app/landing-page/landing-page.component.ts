/*
  Authors: Thanigaiselvan Senthil Shanmugam
*/
import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import {Router} from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {LandingService} from '../services/landingService'
import { ProductPageComponent } from '../product-page/product-page.component';
import { CartService } from '../services/cart-service';
@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
  
})
export class LandingPageComponent implements OnInit {


  gamingProducts;
  studentProducts;
  businessProducts;
  @Output() cartItem: EventEmitter<any> = new EventEmitter();


  constructor(private route:Router,private landingService:LandingService, private cartService: CartService) { }

  ngOnInit() {
    //Service call to retrieve the products to be displayed in the landing page
    this.landingService.landing().subscribe(data=>{
      this.gamingProducts=data.gamingProducts;
      this.studentProducts=data.studentProducts;
      this.businessProducts=data.businessProducts;

      var uid = sessionStorage.getItem('uid')
      if(uid!=undefined && uid != 'null'){
        this.cartService.getCartCount({'uid':uid}).subscribe(data =>{
          var cartCount = data.count;
          this.cartItem.emit(cartCount)
        })
      }
      
    });
  }

  goToProduct(prodId:any)
  {
    this.route.navigate(["","product",prodId])
    
  }

  goToSearch(prodId:any)
  {
    
    this.route.navigate(["","searchResults",prodId])
    
  }
}
