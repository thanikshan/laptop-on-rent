/*
  Authors: Sreyas Naaraayanan Ramanathan; Hari Arunachalam; Srikrishna Sasidharan
*/
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/authentication-service';
import { MatSnackBar } from '@angular/material';
import { CartService } from '../services/cart-service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() loggedIn: EventEmitter<any> = new EventEmitter();

  onInit;
  loginForm: FormGroup;
  constructor(private route: Router, private authService: AuthService, private _snackBar: MatSnackBar, private cartService: CartService) {
    this.loginForm = new FormGroup({
      username: new FormControl('', { validators: [Validators.required, Validators.maxLength(30)] }),
      password: new FormControl('', { validators: [Validators.required, Validators.minLength(6), Validators.maxLength(20)] })
    })
  }

  ngOnInit() {
    this.onInit = false;

  }
  onSubmit() {

  }

  onRegister() {
    this.route.navigate(["", "register"]);

  }

  onLogin() {
    if (this.loginForm.invalid) {
      this.onInit = true;

    }
    else {

      let body = {
        'username': this.loginForm.get('username').value,
        'password': this.loginForm.get('password').value
      }

      this.authService.login(body).subscribe(data => {
        

        if (data.success) {
        sessionStorage.setItem('jwt', data.token);
        sessionStorage.setItem('uid', data.uId);
        sessionStorage.setItem('username', data.username);
        var uid = data.uId
          for (var key in sessionStorage) {
            if (key.indexOf('cartItem') !== -1) {
              var obj = JSON.parse(sessionStorage.getItem(key));
              var startDateString = obj["fromDate"].slice(0, 19).replace('T', ' ');
              var endDateString = obj["toDate"].slice(0, 19).replace('T', ' ');
              var uid = data.uId
              var maxQ = 0
              obj["maxQuantity"].forEach(element => {
                if(element>maxQ){
                  maxQ = element
                }
              });
              this.cartService.addToCart({

                "uid": data.uId,
                "productID": obj["productID"],
                "quantity": obj["quantity"],
                "softwares": obj["softwares"],
                "fromDate": startDateString,
                "toDate": endDateString,
                "maxQuantity": maxQ
              }).subscribe(data => {
                if (data.success) {

                  sessionStorage.removeItem(key)
                  let bd = {
                    'uid': uid
                  }//Check admin and add to session storage - Hari
                  this.authService.fetchProfileDetails(bd).subscribe(da => {
                    if (da && da.success) {
                      data = da.data[0]
                      sessionStorage.setItem('firstName', data.firstName);
                      sessionStorage.setItem('secondName', data.lastName);

                      sessionStorage.setItem('isAdmin', data.isAdmin);
                      this.loggedIn.emit("true")
                      if (data.isAdmin == '1') {
                        this.route.navigate(["", "adminHome"]);
                      } else {
                        this.route.navigate(["", "homepage"]);
                      }

                    }
                  })

                }
              })
            } else {
              let bd = {
                'uid': uid
              }
              this.authService.fetchProfileDetails(bd).subscribe(da => {
                if (da && da.success) {
                  data = da.data[0]
                  sessionStorage.setItem('firstName', data.firstName);
                  sessionStorage.setItem('secondName', data.lastName);

                  sessionStorage.setItem('isAdmin', data.isAdmin);
                  this.loggedIn.emit("true")
                  if (data.isAdmin == '1') {
                    this.route.navigate(["", "adminHome"]);
                  } else {
                    this.route.navigate(["", "homepage"]);
                  }

                }
              })
            }
          }

          // this.route.navigate(["", "homepage"]);

        }else {

          this._snackBar.open(data.message, "Please retry", {
            duration: 1500,
            verticalPosition: 'top'
          });
        }


      })




    }
  }

  onResetPassword() {

    this.route.navigate(["", "resetPassword"]);


  }
}
