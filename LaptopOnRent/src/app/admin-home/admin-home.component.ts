/*
  Authors: Hari Arunachalam
*/
import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { CartService } from '../services/cart-service';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {
  @Output() cartItem: EventEmitter<any> = new EventEmitter();
  constructor(private route:Router,private cartService:CartService) { }

  ngOnInit() {
    var uid = sessionStorage.getItem('uid')
    if(uid!=undefined && uid != 'null'){
      this.cartService.getCartCount({'uid':uid}).subscribe(data =>{
        var cartCount = data.count;
        this.cartItem.emit(cartCount)
      })
    }
    
  
      
  }

  manageUser(){
    this.route.navigate(['','manageUser'])
  }

  addProduct(){
    this.route.navigate(['','addLaptop'])
  }
}
