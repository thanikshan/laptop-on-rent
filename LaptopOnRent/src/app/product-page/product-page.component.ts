/*
  Authors: Thanigaiselvan Senthil Shanmugam - Product Page,Srikrishna Sasidharan - Cart functionality
   
*/
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ReviewDialog } from './review/review.dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormControl } from '@angular/forms';
import { CartService } from '../services/cart-service';
import { ProductService } from '../services/productService';
import { ReviewService } from '../services/reviewService';
import { ViewportScroller } from '@angular/common';


export interface Food {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.css']
})
export class ProductPageComponent implements OnInit {

  @Output() cartItem: EventEmitter<any> = new EventEmitter();

  selectedValue: string;
  @Input('rating') rating: number;
  @Input('starCount') starCount: number = 5;
  @Input('color') color: string = "primary";
  startDate;
  endDate = new Date();
  startDateModel;
  endDateModel;
  quantityModel;
  cartError: boolean = false;
  startDateError: boolean = false;
  endDateError: boolean = false;
  compareDateError: boolean = false;
  checkDate;
  startDateMin;
  endDateMin;
  extra=false;

  Arr = Array;

  //fromdate from database
  fromDate;
  fromDateDatabase;
  productDetails;
  productDetailsDatabase;
  quantityDetailsDatabase: number;
  perDay;
  Company;
  Product;
  Inches;
  productID;
  ScreenResolution;
  Cpu;
  Ram;
  Memory;
  Gpu;
  OpSys;
  Weight;
  productImg;
  tempStartDate;
  tempEndDate;
  prodId: any;
  animal: string;
  name: string;
  maxQuan;
  quan: number;
  //rating loader;
  productStar = 0;
  checkedStars;
  unCheckedStars;

  productReviews;
  showReviews = [];
  hideReviews = [];

  toppings = new FormControl();

  toppingList: string[] = ['Office 365', 'Adobe Photoshop', 'Visual Studio', 'Android Studio', 'FIFA 19', 'Call of Duty'];

  quantities = new FormControl();

  quantityList = [];
  checkH = false;
  constructor(private viewportScroller: ViewportScroller, public dialog: MatDialog, private router: Router, private _snackBar: MatSnackBar, private reviewService: ReviewService, private cartService: CartService, private productService: ProductService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {

    this.prodId = this.activatedRoute.snapshot.paramMap.get("id");

    //Service call to retrieve product details based on prodID
    this.productService.product(this.prodId).subscribe(data => {
      this.fromDateDatabase = data.fromDate;
      this.productDetailsDatabase = data.productDetails;
      this.fromDate = this.fromDateDatabase[0].fromDate
      this.startDateModel = this.fromDate + "T04:00:00"
      this.startDate = new Date(this.startDateModel);
      this.checkDate = new Date();
      this.startDateMin = new Date(this.startDate);
      if (this.startDate < this.checkDate) {
        this.startDateModel = this.checkDate;
        this.startDate = this.checkDate;
        this.startDateMin = new Date(this.startDate);

      }

      this.Company = this.productDetailsDatabase[0].Company;
      this.Product = this.productDetailsDatabase[0].Product;
      this.Inches = this.productDetailsDatabase[0].Inches;
      this.perDay = this.productDetailsDatabase[0].perDay;
      this.productID = this.productDetailsDatabase[0].productID;
      this.ScreenResolution = this.productDetailsDatabase[0].ScreenResolution;
      this.Cpu = this.productDetailsDatabase[0].Cpu;
      this.Ram = this.productDetailsDatabase[0].Ram;
      this.Memory = this.productDetailsDatabase[0].Memory;
      this.Gpu = this.productDetailsDatabase[0].Gpu;
      this.OpSys = this.productDetailsDatabase[0].OpSys;
      this.Weight = this.productDetailsDatabase[0].Weight;
      this.productImg = this.productDetailsDatabase[0].productImg;


      //


      this.endDateMin = new Date();

      this.endDateMin.setDate(this.startDate.getDate() + 1);
      this.endDateMin.setMonth(this.startDate.getMonth());


      //this.endDate.setDate(this.startDate.getDate()+1);
      //this.endDate.setMonth(this.startDate.getMonth());


    });
    //Service call to retrieve the quantity of the product based on prodId
    this.productService.quantity(this.prodId).subscribe(data => {
      this.quantityDetailsDatabase = data.quantity;
      this.quan = this.quantityDetailsDatabase[0].quantity
      var j = 0
      for (var i = 1; i <= parseInt(this.quantityDetailsDatabase[0].quantity); i++) {
        if (i == 4) {
          break;
        }

        this.quantityList[j] = i;
        j++;
      }
      this.maxQuan = this.quantityList

    });
    this.getProductReview();
  }

  getProductReview() {
    this.productReviews=[];
    this.showReviews = [];
    this.hideReviews = [];
    this.productStar=0;
    this.checkH = true;
  
    //Service call to retrieve the reviews for the product
    this.reviewService.getReview(this.prodId).subscribe(data => {
      this.productReviews = data.reviews;

      this.productReviews.forEach(element => {
        this.productStar += element.star;
      });
      for (var i = 0; i < this.productReviews.length; i++) {
        if (i == 2) {
          break;
        }
        this.showReviews[i] = this.productReviews[i];
        this.showReviews[i]['unchecked'] = 5 - this.showReviews[i]['star']
      }
      if (this.productReviews.length > 2) {
        this.checkH = true;
        let j = 0;
        for (var i = 2; i < this.productReviews.length; i++) {

          this.hideReviews[j] = this.productReviews[i];
          this.hideReviews[j]['unchecked'] = 5 - this.hideReviews[j]['star']
          j++;

        }
      } else {
        this.checkH = false;
      }
      console.log(this.hideReviews)
      this.checkedStars = new Array(Math.round(this.productStar / this.productReviews.length));
      this.unCheckedStars = new Array(5 - Math.round(this.productStar / this.productReviews.length));
    });
  }

  //Method to smooth scroll
  public onClick(elementId: string): void {
    this.viewportScroller.scrollToAnchor(elementId);
  }

  addToCart() {
    this.cartError = false;
    this.startDateError = false;
    this.endDateError = false;
    this.compareDateError = false;
    if (!(this.startDateModel && this.startDateModel.value != '')) {
      this.startDateError = true;
    }
    if (!(this.endDateModel && this.endDateModel.value != '')) {
      this.endDateError = true;
    }
    if (((this.quantities.value == '' || this.quantities.value == null))) {
      this.cartError = true;
    }


    this.tempStartDate = new Date(this.startDateModel);
    this.tempEndDate = new Date(this.endDateModel);


    if (this.tempStartDate > this.tempEndDate) {
      this.compareDateError = true;
    }

    if (this.cartError == false && this.startDateError == false && this.endDateError == false && this.compareDateError == false) {

      // to be replaced by fromDate and toDate
      var startDateString = this.startDate.toISOString().slice(0, 19).replace('T', ' ');
      var endDateString = this.endDate.toISOString().slice(0, 19).replace('T', ' ');
      let c = this.toppings.value + '';
      let softwares = c.split(",");
      if (this.toppings.value == undefined) {
        softwares[0] = "Default"
      }
      let user = sessionStorage.getItem('uid')
      var j = 0
      if (user == "null" || user == undefined) {
        var cartItem = {
          "uid": user,
          "Product": this.Product,
          "Company": this.Company,
          "perDay": this.perDay,
          "productID": this.productID,
          "quantity": this.quantities.value,
          "softwares": softwares,
          "fromDate": this.startDate,
          "toDate": this.endDate,
          "prodURL": this.productImg,
          "maxQuantity": this.maxQuan
        };
        var cartCount = +localStorage.getItem('cartCount');
        cartCount = cartCount + 1;
        localStorage.setItem('cartCount', '' + cartCount);
        this.cartItem.emit(cartCount);
        sessionStorage.setItem('cartItem' + this.productID, JSON.stringify(cartItem));
        this.router.navigate(["", "cart"])
      } else {

        this.cartService.addToCart({
          "uid": user,
          "productID": this.productID,
          "quantity": this.quantities.value,
          "softwares": softwares,
          "fromDate": startDateString,
          "toDate": endDateString,
          "maxQuantity": this.quan

        }).subscribe(data => {
          // var cartCount =+localStorage.getItem('cartCount');
          // cartCount = cartCount + 1;
          // localStorage.setItem('cartCount',''+cartCount);
          // this.cartItem.emit(cartCount);

          if (data.success) {
            // sessionStorage.setItem('uid',data.uid)
            this.router.navigate(["", "cart"])
          } else {
            this._snackBar.open("unable to add to cart", "ok", {
              duration: 2000,
            });
          }
        });

      }


    }

  }
  //Method to validate the start date
  addStartEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    if (type == "change") {
      this.startDate.setDate(event.value.getDate())
      this.startDate.setMonth(event.value.getMonth());

      if (event.value.getDate() == 31) {
        this.endDateMin.setDate(event.value.getDate() + 1);
        this.endDateMin.setMonth(event.value.getMonth() + 1);

        // this.endDate.setDate(event.value.getDate()+1);
        // this.endDate.setMonth(event.value.getMonth()+1);
      }
      else if (event.value.getDate() == 30) {
        if (event.value.getMonth() == 3 || event.value.getMonth() == 5 || event.value.getMonth() == 8 || event.value.getMonth() == 10) {
          // this.endDate.setDate(1);  
          // this.endDate.setMonth(event.value.getMonth()+1);
          this.endDateMin.setDate(1);
          this.endDateMin.setMonth(event.value.getMonth() + 1);
        }
        else {
          this.endDateMin.setDate(event.value.getDate() + 1);
          this.endDateMin.setMonth(event.value.getMonth());
          // this.endDate.setDate(event.value.getDate()+1);
          // this.endDate.setMonth(event.value.getMonth());
        }

      }
      else {
        this.endDateMin.setDate(event.value.getDate() + 1);
        this.endDateMin.setMonth(event.value.getMonth());
        // this.endDate.setDate(event.value.getDate()+1);
        // this.endDate.setMonth(event.value.getMonth());
      }
    }

  }
  //Method to validate end date
  addEndEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    if (type == "change") {
      // if(this.startDate.getDate() == 31 )
      // {
      //   this.endDateMin.setDate(this.startDate.getDate()+1);
      //   this.endDateMin.setMonth(this.startDate.getMonth()+1);
      // }
      // else if(this.startDate.getDate() == 30)
      // {
      //   if(this.startDate.getMonth() ==3 || this.startDate.getMonth() ==5 || this.startDate.getMonth()==8 || this.startDate.getMonth()==10)
      //   {
      //     this.endDateMin.setDate(1);  
      //     this.endDateMin.setMonth(this.startDate.getMonth()+1);
      //   }
      //   else
      //   {
      //     this.endDateMin.setDate(this.startDate.getDate());
      //     this.endDateMin.setMonth(this.startDate.getMonth());
      //   }


      // }
      // else{
      //   this.endDateMin.setDate(this.startDate.getDate()+1);
      //   this.endDateMin.setMonth(this.startDate.getMonth());
      //   }

      this.endDate.setDate(event.value.getDate());
      this.endDate.setMonth(event.value.getMonth());

    }

  }
  //Method call to open Review dialog
  openDialog(): void {

    if (sessionStorage.getItem('uid') == null) {
      this._snackBar.open("login to add review", "ok", {
        duration: 2000,
      });
      this.router.navigate(["", "login"])
    }
    else {
      const dialogRef = this.dialog.open(ReviewDialog, {

        data: { starCounting: this.starCount, 'ProductCount': this.prodId }
      });

      dialogRef.afterClosed().subscribe(result => {
        this.animal = result;
        this.getProductReview();
      });

    }
  }

  hideRev(){
    this.extra =true
  }

}
