import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ProductPageComponent } from './product-page/product-page.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { OrdersComponent } from './orders/orders.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { CartComponent } from './cart/cart.component';
import { ChooseAddressComponent } from './choose-address/choose-address.component';
import { ChoosePaymentComponent } from './choose-payment/choose-payment.component';
import { OrderConfirmationComponent } from './order-confirmation/order-confirmation.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { AdminUserDetailsComponent } from './admin-user-details/admin-user-details.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AddLaptopComponent } from './add-laptop/add-laptop.component';
import {ResetPasswordComponent}from './reset-password/reset-password.component';
import {UpdatePasswordDbComponent} from './update-password-db/update-password-db.component';


const routes: Routes = [{path:'homepage',component: LandingPageComponent},
{path:'product/:id',component: ProductPageComponent},
{path:'prod/:id',component: ProductPageComponent},
{path: "login", component: LoginComponent},
{path: "register", component: RegisterComponent},
{path: "order", component: OrdersComponent},
{path: "userProfile", component: UserProfileComponent},
{path: "cart", component: CartComponent},
{path: 'choose-address',component:ChooseAddressComponent},
{path:'choose-payment',component:ChoosePaymentComponent},
{path:'order-confirmation',component:OrderConfirmationComponent},
{path:'searchResults/:id',component:SearchResultComponent},
{path:'searchResult/:id',component:SearchResultComponent},
{path:'adminHome',component:AdminHomeComponent},
{path:'manageUser',component:AdminUserDetailsComponent},
{path:'addLaptop',component:AddLaptopComponent},
{path:'resetPassword',component:ResetPasswordComponent},
{path:'updatePassword/:id',component:UpdatePasswordDbComponent}



//{path:'login',component: LoginComponentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { anchorScrolling: 'enabled' , onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
