import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
//import { SpinnerService } from './services/SpinnerService';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'LaptopOnRent';

  showSpinner: boolean;

  constructor(
    private httpClient: HttpClient,private route:Router){
    this.route.navigate(["","homepage"])
  }

  ngOnInit() {
    //spinnerService: SpinnerService
    //this.spinnerService.visibility =  new BehaviorSubject(false); 
    //sessionStorage.setItem('jwt',null);
    if(sessionStorage.getItem('uid')=='0')
    sessionStorage.setItem('uid', null);
  }

  
  
}

